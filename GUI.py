from tkinter import *
import tkinter.messagebox as messagebox
from PIL import Image
from PIL import ImageTk
import cv2
import numpy as np
import tkinter.filedialog
import image_processing as ip
import timm_detection as td
import brainslice_detection as bd
import interface
import mathematics
import GUI_popups as popups


panel_image = None
image_buffer = list()  # image buffer to save changes
buffer_index = None
image = None
image_path = None
scale_factor = None
label_action = None

kernel = np.ones((5, 5), np.uint8)


def createWindow(window):
    global label_action

    menu_bar = Menu(window)
    window.config(menu=menu_bar)

    # Configures File Menu
    file_menu = Menu(menu_bar, tearoff=0)
    menu_bar.add_cascade(label="File", menu=file_menu)
    file_menu.add_command(label="Open       Ctrl-O", command=openImage)
    window.bind('<Control-KeyPress-o>', openImage)
    file_menu.add_separator()
    file_menu.add_command(label="Save        Ctrl-S", command=saveImage)
    window.bind('<Control-KeyPress-s>', saveImage)
    file_menu.add_command(label="SaveAs     Ctrl-Alt-S", command=saveImageAs)
    window.bind('<Control-Alt-KeyPress-s>', saveImageAs)
    file_menu.add_separator()
    file_menu.add_command(label="Quit", command=window.destroy)

    # Configures Edit Menu
    edit_menu = Menu(menu_bar, tearoff=0)
    menu_bar.add_cascade(label="Edit", menu=edit_menu)
    edit_menu.add_command(label="Undo Change    Ctrl-Z", command=undoChange)
    window.bind('<Control-KeyPress-z>', undoChange)
    edit_menu.add_command(label="Redo Change     Ctrl-Alt-Z", command=redoChange)
    window.bind('<Control-Alt-KeyPress-z>', redoChange)
    edit_menu.add_separator()
    edit_menu.add_command(label="Clear", command=clear)
    edit_menu.add_separator()
    edit_menu.add_command(label="ROI")

    # Configures Image Menu
    image_menu = Menu(menu_bar, tearoff=0)
    menu_bar.add_cascade(label="Image", menu=image_menu)
    flip_menu = Menu(menu_bar, tearoff=0)
    image_menu.add_cascade(label="Flip Image", menu=flip_menu)
    flip_menu.add_command(label="Flip Horizontaly", command=lambda: imgFlip(True, False))
    flip_menu.add_command(label="Flip Verticaly", command=lambda: imgFlip(False, True))
    flip_menu.add_command(label="Flip on both axis", command=lambda: imgFlip(True, True))
    image_menu.add_command(label="Invert Colors", command=invertColors)
    image_menu.add_separator()
    image_menu.add_command(label="Grayscale Image", command=grayScale)
    image_menu.add_separator()
    image_menu.add_command(label="Resize")
    scale_menu = Menu(menu_bar, tearoff=0)
    image_menu.add_cascade(label="Image Scale", menu=scale_menu)
    scale_menu.add_command(label="With Scale Bar", command=getScaleFromBar)
    scale_menu.add_command(label="With Scale Factor", command=getScaleFactor)

    #Configures ROI Menu
    roi_menu = Menu(menu_bar, tearoff=0)
    menu_bar.add_cascade(label="ROI", menu=roi_menu)
    roi_menu.add_command(label="Select rectangle ROI", command=rectangleROI)
    roi_menu.add_command(label="Select rotated rectangle ROI", command=rotatedRectangleROI)
    roi_menu.add_command(label="Select ROI by hand", command=ROIByHand)

    # Configures Quantification Menu
    quantif_menu = Menu(menu_bar, tearoff=0)
    menu_bar.add_cascade(label="Quantification", menu=quantif_menu)
    quantif_menu.add_command(label="Detect Staining", command=detectStaining)
    quantif_menu.add_separator()
    densitometry_menu = Menu(menu_bar, tearoff=0)
    quantif_menu.add_cascade(label="Densitometry", menu=densitometry_menu)
    densitometry_menu.add_command(label="Plot By Rows", command=densitometryByRows)
    densitometry_menu.add_command(label="Plot By Columns", command=densitometryByColumns)
    densitometry_menu.add_command(label="Plot By Rows and Columns", command=densitometryByRowsAndColumns)


    # Configures BrainSlices Menu
    brain_slices_menu = Menu(menu_bar, tearoff=0)
    menu_bar.add_cascade(label="Brain Slices", menu=brain_slices_menu)
    brain_slices_menu.add_command(label="Remove Background", command=removeBackground)
    brain_slices_menu.add_separator()
    brain_slices_menu.add_command(label="Slice Isolator (BETA)", command=sliceIsolator)
    brain_slices_menu.add_separator()
    brain_slices_menu.add_command(label="Define cortical layers position", command=setLayerPos)
    brain_slices_menu.add_command(label="Clear cortical layers position", command=destroyLayerPos)
    brain_slices_menu.add_command(label="Show cortical layers position", command=displayLayerPos)


    window.geometry("300x150")

    label_action = Label(window, text="Ready")
    label_action.pack(side="bottom")

def loadImageInWindow(img):
    global panel_image

    height, width = img.shape[:2]
    if height > 600 or width > 1300:
        img = ip.properResizing(img, 1300, 600)

    try: #Handles exception if image is not BGR (i.e. Grayscaled)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    except cv2.error:
        print("Not an BGR Image")

    img = Image.fromarray(img)  # Converts to PIL format
    img = ImageTk.PhotoImage(img)  # Converts to ImageTk format
    if panel_image is None:
        panel_image = Label(image=img)
        panel_image.image = img
        panel_image.pack(side="left", padx=10, pady=10)
    else:
        panel_image.configure(image=img)
        panel_image.image = img

    autoResizeWindow()


def openImage(*args):
    global image
    global image_path
    global scale_factor
    global image_buffer
    global buffer_index
    global label_action

    label_action['text'] = "Loading image..."
    path = tkinter.filedialog.askopenfilename(title="Open an image file",
                                              filetypes=[('Images', ('*.tif', '*.png', '*.jpg', '*.jpeg'))])
    image_path = path
    image = ip.imgLoad(path)

    # resets buffers and stores new infos
    # buffer_index = 0
    # image_buffer = None
    # image_buffer = list()
    image_buffer.append(image)  # keep image
    if buffer_index == None:
        buffer_index = 0
    else:
        updateBuffer()
        buffer_index += 1
    bd.destroyCortexLayers()
    loadImageInWindow(image)
    scale_factor = None
    label_action['text'] = "Image loaded"


def saveImage(*args):
    global image
    global image_path
    global label_action
    label_action['text'] = "Saving..."
    cv2.imwrite(image_path, image)
    label_action.config(text="Saved at " + str(image_path))



def saveImageAs(*args):
    global image
    global image_path
    global label_action
    path = tkinter.filedialog.asksaveasfile(title="Save As", mode='w').name  # retrieves the chosen path
    if path is None:
        label_action['text'] = "No path chosen"
        return  # ends if path is None
    label_action['text'] = "Saving..."
    cv2.imwrite(path, image)
    image_path = path
    label_action.config(text="Saved at " + str(path))


def undoChange(*args):
    global image
    global image_path
    global scale_factor
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == 0:
        return
    buffer_index -= 1
    image = image_buffer[buffer_index]
    loadImageInWindow(image)
    bd.destroyCortexLayers()
    label_action['text'] = "Change undone"


def redoChange(*args):
    global image
    global image_path
    global scale_factor
    global image_buffer
    global buffer_index
    global label_action 

    if buffer_index == len(image_buffer) - 1:
        return
    buffer_index += 1
    image = image_buffer[buffer_index]
    loadImageInWindow(image)
    bd.destroyCortexLayers()
    label_action['text'] = "Change redone"

def clear():
    global image
    global image_path
    global scale_factor
    global image_buffer
    global buffer_index
    global panel_image
    global label_action

    image = None
    image_path = None
    scale_factor = None
    image_buffer = None
    image_buffer = list()
    buffer_index = None
    panel_image.pack_forget()
    panel_image = None
    bd.destroyCortexLayers()
    window.geometry("300x150")
    label_action['text'] = "Cleared"


def invertColors():
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return
    label_action['text'] = "Inverting..."
    img = ip.invertColors(image)
    bd.destroyCortexLayers()
    updateBuffer()
    buffer_index += 1
    image = img
    image_buffer.append(img)
    loadImageInWindow(img)
    label_action['text'] = "Colors Inverted"


def imgFlip(horizontal, vertical):
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return
    label_action['text'] = "Flipping..."
    img = ip.flipImage(image, horizontal, vertical)
    bd.destroyCortexLayers()
    updateBuffer()
    buffer_index += 1
    image = img
    image_buffer.append(img)
    loadImageInWindow(img)
    label_action['text'] = "Image Flipped"

def grayScale():
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return
    label_action['text'] = "Grayscaling..."
    img = ip.grayScale(image)
    updateBuffer()
    bd.destroyCortexLayers()
    buffer_index += 1
    image = img
    image_buffer.append(img)
    loadImageInWindow(img)
    label_action['text'] = "Image grayscaled"

def getScaleFromBar():
    global image
    global scale_factor
    global label_action

    if buffer_index == None:
        return
    points = interface.selectLine(image, "Select the two points of the Scale Bar")
    popup = popups.scaleBarPopupWindow(window)
    window.wait_window(popup.top)
    entry = int(popups.returnEntryValue(popup))
    scale_factor = mathematics.getScaleFromScaleBar(points, entry)
    print("Scale Factor = " + str(scale_factor))
    label_action['text'] = "Scale Factor = " + str(scale_factor) + "(µm/pixel)"

def getScaleFactor():
    global image
    global scale_factor
    global label_action

    if buffer_index == None:
        return
    popup = popups.scaleFactorPopupWindow(window)
    window.wait_window(popup.top)
    entry = int(popups.returnEntryValue(popup))
    scale_factor = entry
    label_action['text'] = "Scale Factor = " + str(scale_factor) + "(µm/pixel)" 

def rectangleROI():
    global image
    global image_buffer
    global buffer_index

    if buffer_index == None:
        return
    height, width = image.shape[:2]
    if height > 600 or width > 1300:
        img = ip.properResizing(image, 1300, 600)
    else:
        img = image

    coefficient = ip.properResizingCoefficient(image, img)
    # Selects ROI
    showCrosshair = False
    fromCenter = False
    r = cv2.selectROI("Select rectangle ROI and press Enter", img, fromCenter, showCrosshair)
    label_action['text'] = "Cropping..." 
    i=0
    new_r = np.zeros(4)
    while i < 4:
        new_r[i] = int(r[i] * coefficient)
        i += 1
    r = new_r
    # Crops image
    cropped = image[int(r[1]):int(r[1] + r[3]), int(r[0]):int(r[0] + r[2])]
    cv2.destroyWindow("Select rectangle ROI and press Enter")

    updateBuffer()
    bd.destroyCortexLayers()
    buffer_index += 1
    image = cropped
    image_buffer.append(cropped)
    loadImageInWindow(cropped)
    label_action['text'] = "Cropped"

def rotatedRectangleROI():
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return
    label_action['text'] = "Cropping..."
    ROI = interface.selectRoiByHand(image, "Select ROI by doing three left clicks to define a rectangle", True)
    center, angle, width, height = mathematics.getRotatedRectangleInfos(ROI)
    img = ip.extractRotatedRectangle(image, (center[0], center[1]), angle, width, height)

    updateBuffer()
    bd.destroyCortexLayers()
    buffer_index += 1
    image = img
    image_buffer.append(img)
    loadImageInWindow(img)
    label_action['text'] = "Cropped"

def ROIByHand():
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return

    ROI = interface.selectRoiByHand(image, "Select ROI by left clicks and press Enter", False)
    label_action['text'] = "Cropping..."
    img = ip.cropPolygon(image, ROI, True)

    updateBuffer()
    bd.destroyCortexLayers()
    buffer_index += 1
    image = img
    image_buffer.append(img)
    loadImageInWindow(img)
    label_action['text'] = "Cropped"

def detectStaining():
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return
    label_action['text'] = "Detecting..."
    img, inverted = td.detectStaining(image)
    if inverted:
        img = ip.invertColors(img)
    updateBuffer()
    buffer_index += 1
    image = img
    image_buffer.append(img)
    loadImageInWindow(img)
    label_action['text'] = "Staining detected"

def densitometryByRows():
    global image
    global buffer_index
    global scale_factor
    global label_action

    if buffer_index == None:
        return

    img, inverted = td.detectStaining(image)
    label_action['text'] = "Detecting..."
    mean_intensity_by_rows, rows = td.getMeanStainingIntensityByRow(img)
    label_action['text'] = "Plotting..."
    if bd.cortex_layers_pos != None:
        popup = popups.densitometryByLayersPopupWindow(window)
        window.wait_window(popup.top)
        img, figure, analysis = popups.returnDensitometryWindowValues(popup)
        mathematics.plotQuantificationByRows(mean_intensity_by_rows, rows, image, scale_factor, img, figure, analysis)
    else:
        mathematics.plotQuantificationByRows(mean_intensity_by_rows, rows, image, scale_factor, False, False, False)
    label_action['text'] = "Graph plotted"


def densitometryByColumns():
    global image
    global buffer_index
    global scale_factor
    global label_action

    if buffer_index == None:
        return
    img, inverted = td.detectStaining(image)
    label_action['text'] = "Detecting..."
    mean_intensity_by_col, columns = td.getMeanStainingIntensityByColumns(img)
    label_action['text'] = "Plotting..."
    mathematics.plotQuantificationByColumns(mean_intensity_by_col, columns, image, scale_factor)
    label_action['text'] = "Graph plotted"

def densitometryByRowsAndColumns():
    global image
    global buffer_index
    global scale_factor
    global label_action 

    if buffer_index == None:
        return
    img, inverted = td.detectStaining(image)
    label_action['text'] = "Detecting..."
    mean_intensity_by_rows, rows = td.getMeanStainingIntensityByRow(img)
    mean_intensity_by_col, columns = td.getMeanStainingIntensityByColumns(img)
    label_action['text'] = "Plotting..."
    if bd.cortex_layers_pos != None:
        popup = popups.densitometryByLayersPopupWindow(window)
        window.wait_window(popup.top)
        popup_values = popups.returnDensitometryWindowValues(popup)
        img_bool = popup_values[0]
        figure = popup_values[1]
        analysis = popup_values[2]
        mathematics.plotQuantificationByRowsAndColumns(mean_intensity_by_rows, rows, mean_intensity_by_col, columns, image, scale_factor, img_bool, figure, analysis)
    else:
        mathematics.plotQuantificationByRowsAndColumns(mean_intensity_by_rows, rows, mean_intensity_by_col, columns, image, scale_factor, False, False, False)
    label_action['text'] = "Graph plotted"

def removeBackground():
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return
    title = "Select rectangle of Background and press Enter"
    background = interface.resizeAndSelectROI(image, title)
    label_action['text'] = "Removing Background..."
    while label_action['text'] != "Removing Background...":
        pass
    limits = bd.findHighestLowestRGBValues(background)
    img = bd.removeBackground(image, limits, white=True)

    updateBuffer()
    bd.destroyCortexLayers()
    buffer_index += 1
    image = img
    image_buffer.append(img)
    loadImageInWindow(img)
    label_action['text'] = "Background Removed"

def sliceIsolator():
    global image
    global image_buffer
    global buffer_index
    global label_action

    if buffer_index == None:
        return
    path = tkinter.filedialog.askdirectory(title="Save slices at:")  # retrieves the chosen path
    bd.processImg(image, path, True)
    label_action['text'] = "Slice Isolation..."
    while label_action['text'] != "Slice Isolation...":
        pass

    label_action['text'] = "Slices saved at " + path


def setLayerPos():
    global image
    global image_buffer
    global buffer_index
    global label_action
    label_action['text'] = "Setting Cortical Layers position..."
    if messagebox.askyesno("Cortical Layers", "Fuse Layers 2/3?"):
        bd.setCortexLayers(image, 1, 6, True)
    else:
        bd.setCortexLayers(image, 1, 6, False)
    label_action['text'] = "Cortical Layers position set"

def destroyLayerPos():
    bd.destroyCortexLayers()
    label_action['text'] = "Cortical Layers position cleared"

def displayLayerPos():
    if bd.cortex_layers_pos == None:
        return
    image_copy = image
    image_copy = bd.printCortexLayersOnImage(image_copy, True)
    loadImageInWindow(image_copy)


def updateBuffer():
    global image_buffer
    global buffer_index
    #print(len(image_buffer))
    new_buffer = image_buffer[0:buffer_index + 1]
    image_buffer = new_buffer
    #print(len(image_buffer))

def autoResizeWindow():
    window.geometry("")



window = Tk()  # Création de la fenêtre racine
window.title("TSS Quantifyer")
createWindow(window)
window.mainloop()  # Lancement de la boucle principale
