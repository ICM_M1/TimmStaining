from tkinter import *
from tkinter import ttk
import sys


def returnEntryValue(object):
    return object.value

def returnDensitometryWindowValues(object):
    return [object.img, object.figure, object.analysis]

class scaleBarPopupWindow(object):
    def __init__(self, master):
        top = self.top = Toplevel(master)
        self.label = Label(top, text="Enter value in µm")
        self.entry = Entry(top)
        self.button = Button(top, text='Validate', command=self.validateInput)
        self.label.pack()
        self.entry.pack()
        self.button.pack()

    def validateInput(self):
        self.value = self.entry.get()
        self.top.destroy()

class scaleFactorPopupWindow(object):
    def __init__(self, master):
        top = self.top = Toplevel(master)
        self.label = Label(top, text="Enter the scale factor (µm/pixel)")
        self.entry = Entry(top)
        self.button = Button(top, text='Validate', command=self.validateInput)
        self.label.pack()
        self.entry.pack()
        self.button.pack()

    def validateInput(self):
        self.value = self.entry.get()
        self.top.destroy()

class densitometryByLayersPopupWindow(object):
    def __init__(self, master):
        top = self.top = Toplevel(master)
        self.display_layers_img = IntVar()
        self.display_layers_figure = IntVar()
        self.display_layers_analysis = IntVar()
        self.display_layers_img_checkbutton = Checkbutton(top, text="Display cortical layers on image", variable=self.display_layers_img, onvalue=1, offvalue=0)
        self.display_layers_figure_checkbutton = Checkbutton(top, text="Display cortical layers on figure", variable=self.display_layers_figure, onvalue=1, offvalue=0)
        self.display_layers_analysis_checkbutton = Checkbutton(top, text="Display analysis by cortical layers", variable=self.display_layers_analysis, onvalue=1, offvalue=0)
        self.button = Button(top, text='Validate', command=self.validateInput)
        self.display_layers_img_checkbutton.pack()
        self.display_layers_figure_checkbutton.pack()
        self.display_layers_analysis_checkbutton.pack()
        self.button.pack()

    def validateInput(self):
        if self.display_layers_img.get() == 1:
            self.img = True
        else:
            self.img = False

        if self.display_layers_figure.get() == 1:
            self.figure = True
        else:
            self.figure = False

        if self.display_layers_analysis.get() == 1:
            self.analysis = True
        else:
            self.analysis = False

        self.top.destroy()
