import os
import numpy as np
import argparse
import cv2
import datetime
import interface
import image_processing as ip

cortex_layers = None
cortex_layers_pos = None


def removeBackground(image, background, white):
    img_copy = image
    lower = ip.npArrayGeneration(background[0])
    upper = ip.npArrayGeneration(background[1])
    background_mask = cv2.inRange(img_copy, lower, upper)
    black_mask = cv2.bitwise_and(img_copy, img_copy, mask=background_mask)
    black_mask = cv2.bitwise_not(black_mask)
    black_mask = ip.blackEnhancement(black_mask)
    output = ip.applyBlackMask(img_copy, black_mask, white)
    #cv2.imshow("Background removed", output)
    #cv2.waitKey(0)
    return output

def selectAndRemoveBackground(image, white):
    img_copy = image
    #image_resized = ip.properResizing(image, 1300, 700)
    background = interface.selectROI(img_copy, "Select Background sample")
    limits = findHighestLowestRGBValues(background)
    img = removeBackground(img_copy, limits, white)
    return img

def findHighestLowestRGBValues(image):
    minR = image[..., 0].min()
    minG = image[..., 1].min()
    minB = image[..., 2].min()
    maxR = image[..., 0].max()
    maxG = image[..., 1].max()
    maxB = image[..., 2].max()
    lowest = [minG, minB, minR]
    highest = [maxG, maxB, maxR]
    print(lowest)
    print(highest)
    return [lowest, highest]



def isolateSlices(image, contours, dir, white):
    dirname = datetime.datetime.today().strftime('%d-%m-%Y')
    if not ip.checkifFolderExists(dirname):
        os.mkdir(dirname)
    height, width = image.shape[:2]
    i = 0
    while os.path.isfile(dir + '/' + dirname + '/' + str(i)+'.png'):
        i += 1
    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        if w > (width / 10) and h > (height / 10):
            cropped = image[y:y + h, x:x + w]
            background = [[230, 230, 230], [255, 255, 255]]
            cropped = removeBackground(cropped, background, white)
            cv2.imwrite((os.path.join(dir + '/' + dirname, str(i)+'.png')), cropped)
            print((os.path.join(dir + '/' + dirname, str(i)+'.png')))
            cropped_resized = ip.properResizing(cropped, 1300, 700)
            cv2.imshow("Cropped", cropped_resized)
            cv2.waitKey(0)
            i += 1

def processImg(image,path, white):
    image = selectAndRemoveBackground(image, white)
    image_copy = image.copy()
    gray = ip.grayScale(image)
    #gray_display = cv2.resize(gray, (200, 500))
    #ip.imgShow([gray_display])

    kernel = np.ones((5, 5), np.uint8)
    edged = ip.edgeDetectionCanny(gray, 1, 120)
    dilation = ip.imgDilation(edged, kernel)
    closing = ip.imgClosing(dilation, kernel)
    #ip.imgShow([cv2.resize(closing, (200, 500))])
    image_resized = ip.properResizing(closing, 1300, 700)
    ip.imgShow([image_resized])

    (image, contours, hierarchy) = ip.countoursDetection(closing)
    if white:
        cont = cv2.drawContours(image_copy, contours, -1, (255, 255, 255), 1, cv2.LINE_AA)
        isolateSlices(cont, contours, path, True)
    else:
        cont = cv2.drawContours(image_copy, contours, -1, (0, 0, 0), 1, cv2.LINE_AA)
        mask = np.zeros(cont.shape[:2], dtype="uint8") * 255

        # Draw the contours on the mask
        cv2.drawContours(mask, contours, -1, (255, 255, 255), -1)

        # remove the contours from the image and show the resulting images
        img = cv2.bitwise_and(cont, cont, mask=mask)
        isolateSlices(img, contours, path, False)

#Allows the user to define the position of cortical layers on an image
def setCortexLayers(cortex_img, first_layer, last_layer, fuse_layers):
    global cortex_layers_pos
    global cortex_layers

    cortex_layers_pos = None
    cortex_layers = None
    cortex_layers_pos = list()
    cortex_layers = list()
    i = first_layer
    while i <= last_layer:
        cortex_layers.append(i)
        i +=1
    if fuse_layers:
        del cortex_layers[cortex_layers.index(3)]
        cortex_layers[cortex_layers.index(2)] = "2/3"

    i = first_layer
    cortex_layers_pos.append(interface.selectPixelCoordinates(cortex_img, "Select the begining of layer " + str(i)))
    cortex_layers_pos.append(interface.selectPixelCoordinates(cortex_img, "Select the end of layer " + str(i)))
    i += 1
    while i <= last_layer:
        if i == 2 and fuse_layers:
            cortex_layers_pos.append(interface.selectPixelCoordinates(cortex_img, "Select the end of layer 2/3"))
            i += 2
        else:
            cortex_layers_pos.append(interface.selectPixelCoordinates(cortex_img, "Select the end of layer " + str(i)))
            i += 1

def printCortexLayersOnImage(image, text_bool):
    global cortex_layers
    global cortex_layers_pos

    image_copy = image
    height, width = image.shape[:2]
    i = 0
    font_scale = determineFontScale(image_copy)
    line_size = determineLineSize(image_copy)
    text_thickness = determineTextThickness(image_copy)
    if cortex_layers_pos == None:
        return
    while i < (len(cortex_layers_pos)-1):
        y_pos = cortex_layers_pos[i][1]
        ip.drawLine(image_copy, 0, y_pos, width, y_pos, [255, 255, 255], line_size)
        if text_bool:
            text = convertLayerNumber(cortex_layers[i])
            text_pos = int(y_pos + ((cortex_layers_pos[i+1][1]-y_pos)/2) + (1/2 * (font_scale * 25)))
            if (cortex_layers_pos[i+1][1]-y_pos) < (font_scale * 25) + 4:
                new_font_scale = (cortex_layers_pos[i+1][1] - y_pos - 4) / 25
                new_text_pos = int(y_pos + ((cortex_layers_pos[i + 1][1] - y_pos) / 2) + (1 / 2 * (new_font_scale * 25)))
                ip.writeTextOnImage(image_copy, text, 10, new_text_pos, new_font_scale, (255, 255, 255), text_thickness)
            else:
                ip.writeTextOnImage(image_copy, text, 10, text_pos, font_scale, (255, 255, 255), text_thickness)
        i += 1
    y_pos = cortex_layers_pos[i][1]
    ip.drawLine(image_copy, 0, y_pos, width, y_pos, [255, 255, 255], line_size)
    return image_copy

def destroyCortexLayers():
    global cortex_layers
    global cortex_layers_pos
    cortex_layers = None
    cortex_layers_pos = None

def convertLayerNumber(number):
    range = np.arange(1, 7, 1)
    if number == "2/3":
        text = 'II/III'
        return text
    if number not in range:
        return
    text = ''
    if number == 1:
        text = 'I'
    elif number == 2:
        text = 'II'
    elif number == 3:
        text = 'III'
    elif number == 4:
        text = 'IV'
    elif number == 5:
        text = 'V'
    elif number == 6:
        text = 'VI'
    return text

def determineFontScale(image):
    height, width = image.shape[:2]
    pixels_per_letter = 25 #25 pixels per upper case letter with font_scale = 1
    font_size = height / 30 #locks the proportion to 1/30 of the the image height
    font_scale = font_size / pixels_per_letter
    return font_scale

def determineLineSize(image):
    height, width = image.shape[:2]
    if height <= 350:
        line_size = 1
    else:
        line_size = int(height / 350)
    return line_size

def determineTextThickness(image):
    height, width = image.shape[:2]
    if height <= 350:
        text_thickness = 2
    else:
        text_thickness = int(height / 350)
    return text_thickness

def analyseDensitometryByLayers(mean_intensity_by_rows, rows):
    densitometry_by_layers = np.zeros(len(cortex_layers))
    i = 0
    while i < len(cortex_layers):
        densitometry = 0
        start = cortex_layers_pos[i][1]
        end = cortex_layers_pos[i+1][1]
        range = end - start
        while start < end:
            densitometry += mean_intensity_by_rows[np.where(rows == start)]
            start += 1
        densitometry /= range
        densitometry_by_layers[i] = densitometry
        i += 1
    return densitometry_by_layers








#Argument Parser
# ap = argparse.ArgumentParser()
# ap.add_argument("-i", "--image", help="Image path")
# ap.add_argument('--white', help='Remove background in White', action='store_true')
# args = vars(ap.parse_args())
# processImg(args["image"], args["white"])
