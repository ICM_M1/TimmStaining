import os
import numpy as np
import math
import cv2
import tifffile as tif

####################BASIC IMAGE PROCESSING FUNCTIONS################
def imgLoad(path):
    #Loads image with tifffile.py if it is a TIFF file, otherwise with OpenCV
    if ".tif" in path:
        image = tif.imread(path)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    else:
        image = cv2.imread(path, -1)
    height, width = image.shape[:2]

    #Resize image if it can't be buffered by OpenCV ImageWrapper
    if height > 32766 or width > 32766:
        image = properResizing(image, 32766, 32766)

    return image

def imgShow(images):
    cv2.imshow("images", np.hstack(images))
    cv2.resizeWindow('image', 600, 600)
    cv2.waitKey(0)

def npArrayGeneration(tuple):
    array = np.array(tuple, dtype = "uint8")
    return array

def grayScale(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    return gray

def invertColors(image):
    image = cv2.bitwise_not(image)
    return image

def flipImage(image, horizontal, vertical):
    if horizontal and vertical:
        flipped = cv2.flip(image, -1)
    elif horizontal:
        flipped = cv2.flip(image, 1)
    elif vertical:
        flipped = cv2.flip(image, 0)
    return flipped

def properResizing(image, xmax, ymax):
    height, width = image.shape[:2]
    r = width / height
    rmax = xmax / ymax
    if (r > rmax):
        image_resized = cv2.resize(image, (xmax, round((1/r)*xmax)))
    else:
        image_resized = cv2.resize(image, (round(r * ymax), ymax))
    return image_resized

def properResizingCoefficient(image, image_resized):
    height, width = image.shape[:2]
    height_resized, width_resized = image_resized.shape[:2]
    coefficient = width/width_resized
    return coefficient


###################CONTOURS DETECTION####################
def edgeDetectionCanny(image, lower, upper):
    edged = cv2.Canny(image, lower, upper)
    return edged

def imgDilation(image, kernel):
    dilation = cv2.dilate(image, kernel, iterations=1)
    return dilation

def imgClosing(image, kernel):
    closing = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)
    return closing

def countoursDetection(image):
    (img, countours, hierarchy) = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return(img, countours, hierarchy)




###################COLOR PROCESSING######################
def colorDetection(image, lower, upper):
    mask = cv2.inRange(image, lower, upper)
    if lower[0]==0 and lower[1]==0 and lower[2]==0:
        mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        black_mask = blackEnhancement(mask)
        output = applyBlackMask(image, black_mask, True)
    else:
        output = cv2.bitwise_and(image, image, mask=mask)
    return output

def quantifyWhitePixels(image):
    nb_pixels = np.count_nonzero((image == [255, 255, 255]).all(axis=2))
    return nb_pixels

def quantifyBlackPixels(image):
    nb_pixels = np.count_nonzero((image == [0, 0, 0]).all(axis=2))
    return nb_pixels

def quantifyPixelsByColor(image, color):
    nb_pixels = np.count_nonzero((image == [color[0], color[1], color[2]]).all(axis=2))
    return nb_pixels

def quantifyNonColoredPixels(image, color):
    nb_pixels = np.count_nonzero((image != [color[0], color[1], color[2]]).all(axis=2))
    return nb_pixels

#Converts all non white pixels into black pixels
def whiteEnhancement(image):
    image_copy = image
    image_copy[np.where((image_copy!=[0, 0, 0]).all(axis=2))] = [255, 255, 255]
    return image_copy

#Converts all non black pixels into white pixels
def blackEnhancement(image):
    image_copy = image
    image_copy[np.where((image_copy != [255, 255, 255]).all(axis=2))] = [0, 0, 0]
    return image_copy

#Transposes black pixels of an image in another one
#image = image to get filled with black pixels
#mask = image representing the black pixels
#white = BOOL(if true then black pixels from mask will be white in image)
def applyBlackMask(image, mask, white):
    indices = np.where((mask == [0, 0, 0]).all(axis=2))
    coordinates = list(zip(indices[0], indices[1]))
    for pixel in coordinates:
        if white:
            image[pixel[0], pixel[1]] = [255, 255, 255]
        else:
            image[pixel[0], pixel[1]] = [0, 0, 0]
    return image

######################GEOMETRY###########################

def drawLine(image, x1, y1, x2, y2, color, thickness):
    cv2.line(image, (x1, y1), (x2, y2), (color[0], color[1], color[2]), thickness)

def drawPolygon(image, points, color, thickness):
    nb_points = len(points)
    i = 0
    while i < nb_points-1:
        drawLine(image, points[i][0], points[i][1], points[i+1][0], points[i+1][1], (color[0], color[1], color[2]), thickness)
        i += 1
    drawLine(image, points[i-1][0], points[i-1][1], points[i][0], points[i][1], (color[0], color[1], color[2]), thickness)
    drawLine(image, points[i][0], points[i][1], points[0][0], points[0][1], (color[0], color[1], color[2]), thickness)

def cropRotatedRectangle(image, center, angle, width, height):
   shape = image.shape[:2]
   angle *= 180 / np.pi  # converts radians in degrees
   matrix = cv2.getRotationMatrix2D(center=center, angle=angle, scale=1)
   image = cv2.warpAffine(src=image, M=matrix, dsize=shape)
   x = int(center[0] - width/2)
   y = int(center[1] - height/2)
   cropped_image = image[y:y+height, x:x+width]
   return cropped_image

def cropPolygon(image, points, white):
    image_copy = image
    mask = np.zeros(image.shape, dtype=np.uint8)
    roi_corners = np.array([points], dtype=np.int32)
    channel_count = image.shape[2]  # i.e. 3 or 4 depending on your image
    ignore_mask_color = (255,) * channel_count
    cv2.fillPoly(mask, roi_corners, ignore_mask_color)
    #imgShow([mask])
    masked_image = applyBlackMask(image_copy, mask, white)
    centre, dimensions, theta = cv2.minAreaRect(np.array([points], dtype=np.int32))
    width = int(dimensions[0])
    height = int(dimensions[1])

    rect = cv2.minAreaRect(np.array([points], dtype=np.int32))
    box = cv2.boxPoints(rect)
    box = np.int0(box)

    M = cv2.moments(box)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])
    cropped = extractRotatedRectangle(masked_image, (cx, cy), theta+90, height, width)
    return cropped

def extractRotatedRectangle(image, center, angle, width, height):
    angle *= 180 / np.pi #converts radians in degrees
    print(angle)
    if 90 < angle <= 180:
        angle = angle - 90
        width, height = height, width
    angle *= math.pi / 180  # convert to radians
    v_x = (math.cos(angle), math.sin(angle))
    v_y = (-math.sin(angle), math.cos(angle))
    s_x = center[0] - v_x[0] * (width / 2) - v_y[0] * (height / 2)
    s_y = center[1] - v_x[1] * (width / 2) - v_y[1] * (height / 2)
    mapping = np.array([[v_x[0], v_y[0], s_x], [v_x[1], v_y[1], s_y]])
    cropped = cv2.warpAffine(image, mapping, (width, height), flags=cv2.WARP_INVERSE_MAP, borderMode=cv2.BORDER_REPLICATE)
    return cropped


#####################MISC#################################

def checkifFolderExists(dir):
    return os.path.exists(dir)

def writeTextOnImage(image, text, x, y, size, color, thickness):
    image_copy = image
    cv2.putText(image_copy, text, (x, y), cv2.FONT_HERSHEY_COMPLEX, size, (color[0], color[1], color[2]), thickness, cv2.LINE_AA)
    return image_copy
