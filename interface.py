import cv2
import numpy as np
import image_processing as ip
import mathematics

left_clicks = list()

def selectPixelEvent(event, x, y, flags, params):
    global left_clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        position = [x, y]
        left_clicks.append(position)

def selectPixelPosition(image, title):
    cv2.setMouseCallback(title, selectPixelEvent)
    leftcliks_number = len(left_clicks)
    while (len(left_clicks) == leftcliks_number):
        cv2.waitKey(0)
    length = len(left_clicks)
    pixel = left_clicks[length-1]
    return pixel

def selectPixelColor(image, title):
    image_resized = ip.properResizing(image, 1200, 600)
    cv2.imshow(title, image_resized)
    cv2.setMouseCallback(title, selectPixelEvent)
    leftcliks_number = len(left_clicks)
    while (len(left_clicks) == leftcliks_number):
        cv2.waitKey(0)
    length = len(left_clicks)
    pixel = left_clicks[length-1]
    G = image_resized[pixel[1], pixel[0]][0]
    B = image_resized[pixel[1], pixel[0]][1]
    R = image_resized[pixel[1], pixel[0]][2]
    color = [G, B, R]
    cv2.destroyWindow(title)
    return color

def selectPixelCoordinates(image, title):
    image_resized = ip.properResizing(image, 1200, 600)
    cv2.imshow(title, image_resized)
    cv2.setMouseCallback(title, selectPixelEvent)
    leftcliks_number = len(left_clicks)
    while (len(left_clicks) == leftcliks_number):
        cv2.waitKey(0)
    length = len(left_clicks)
    pixel = left_clicks[length - 1]
    coefficient = ip.properResizingCoefficient(image, image_resized)
    new_pixel = [round(pixel[0] * coefficient), round(pixel[1] * coefficient)]
    cv2.destroyWindow(title)
    return new_pixel


def selectLine(image, title):
    points = list()
    cv2.namedWindow(title)
    image_resized = ip.properResizing(image, 1200, 600)
    cv2.imshow(title, image_resized)
    cv2.setMouseCallback(title, selectPixelEvent)

    # Waits for start point and append its coordinates to the list
    leftcliks_number = len(left_clicks)
    while (len(left_clicks) == leftcliks_number):
        cv2.waitKey(1)
    start = left_clicks[len(left_clicks) - 1]
    points.append(start)

    # Waits for the second point, draws the line and append its coordinates to the list
    leftcliks_number = len(left_clicks)
    while (len(left_clicks) == leftcliks_number):
        cv2.waitKey(1)
    length = len(left_clicks)
    next = left_clicks[length - 1]
    ip.drawLine(image_resized, start[0], start[1], next[0], next[1], [0, 255, 0], 5)
    cv2.imshow(title, image_resized)
    points.append(next)
    coefficient = ip.properResizingCoefficient(image, image_resized)
    new_points = transformCoordinatesWithResiszingCoefficient(points, coefficient)
    print(points)
    print(new_points)
    print(coefficient)


    # Destroys Window and returns points
    cv2.destroyWindow(title)
    return new_points

def selectROI(image, title):
    # Selects ROI
    showCrosshair = False
    fromCenter = False
    r = cv2.selectROI(title, image, fromCenter, showCrosshair)
    # Crops image
    cropped = image[int(r[1]):int(r[1] + r[3]), int(r[0]):int(r[0] + r[2])]
    cv2.destroyWindow(title)
    return cropped

def resizeAndSelectROI(image, title):
    height, width = image.shape[:2]
    if height > 600 or width > 1300:
        img = ip.properResizing(image, 1300, 600)
    else:
        img = image

    coefficient = ip.properResizingCoefficient(image, img)
    # Selects ROI
    showCrosshair = False
    fromCenter = False
    r = cv2.selectROI(title, img, fromCenter, showCrosshair)
    i = 0
    new_r = np.zeros(4)
    while i < 4:
        new_r[i] = int(r[i] * coefficient)
        i += 1
    r = new_r
    # Crops image
    cropped = image[int(r[1]):int(r[1] + r[3]), int(r[0]):int(r[0] + r[2])]
    cv2.destroyWindow(title)
    return cropped


def selectRoiByHand(image, title, rotated_rectangle):
    points = list()
    cv2.namedWindow(title)
    image_resized = ip.properResizing(image, 1200, 600)
    cv2.imshow(title, image_resized)
    cv2.setMouseCallback(title, selectPixelEvent)

    #Waits for start point and append its coordinates to the list
    leftcliks_number = len(left_clicks)
    while (len(left_clicks) == leftcliks_number):
        cv2.waitKey(1)
    start = left_clicks[len(left_clicks) - 1]
    points.append(start)

    # Waits for the second point, draws the line and append its coordinates to the list
    leftcliks_number = len(left_clicks)
    while (len(left_clicks) == leftcliks_number):
        cv2.waitKey(1)
    length = len(left_clicks)
    next = left_clicks[length-1]
    ip.drawLine(image_resized, start[0], start[1],  next[0], next[1], [0, 255, 0], 5)
    cv2.imshow(title, image_resized)
    points.append(next)
    previous = next

    if rotated_rectangle:
        # Waits for the third point, draws the line and append its coordinates to the list
        leftcliks_number = len(left_clicks)
        while (len(left_clicks) == leftcliks_number):
            cv2.waitKey(1)
        length = len(left_clicks)
        previous = next
        next = left_clicks[length - 1]
        ip.drawLine(image_resized, previous[0], previous[1], next[0], next[1], [0, 255, 0], 5)
        cv2.imshow(title, image_resized)
        points.append(next)
        points = mathematics.calculateRectangleFrom3Points([points])
        coefficient = ip.properResizingCoefficient(image, image_resized)
        new_points = transformCoordinatesWithResiszingCoefficient(points, coefficient)
        cv2.destroyWindow(title)
        return new_points


    else:
        # Loop to retrieve all the points and draw the lines until Enter is pressed
        leftcliks_number = len(left_clicks)
        while (cv2.waitKeyEx(1) != 13):
            if (len(left_clicks) != leftcliks_number):
                length = len(left_clicks)
                next = left_clicks[length - 1]
                ip.drawLine(image_resized, previous[0], previous[1], next[0], next[1], [0, 255, 0], 5)
                cv2.imshow(title, image_resized)
                previous = next
                leftcliks_number = len(left_clicks)
                points.append(next)
        #if last point different to start point it draws a line between
        if (next[0] != start[0] or next[1] != start[1]):
            ip.drawLine(image_resized, next[0], next[1], start[0], start[1], [0, 255, 0], 5)
            cv2.imshow(title, image_resized)
        coefficient = ip.properResizingCoefficient(image, image_resized)
        new_points = transformCoordinatesWithResiszingCoefficient(points, coefficient)
        cv2.destroyWindow(title)
        return new_points


def selectScale(image, value):
    points = selectLine(image, "Select the two points of the Scale Bar")
    scale_factor = mathematics.getScaleFromScaleBar(points, value)
    return scale_factor


#Modify the coordinates in a list from a resized image(x2,y2) to transpose it in the original image(x1,y1)
def transformCoordinatesWithResiszingCoefficient(points, coefficient):
    new_points = list()
    for point in points:
        new_x = round(point[0] * coefficient)
        new_y = round(point[1] * coefficient)
        new_points.append([new_x, new_y])
    return new_points

#Modify the coordinates in a list from a resized image(x2,y2) to transpose it in the original image(x1,y1)
def transformCoordinatesAfterResizing(points, x1, y1, x2, y2):
    new_points = list()
    for point in points:
        new_x = round(point[0] * x1 / x2)
        new_y = round(point[1] * y1 / y2)
        new_points.append([new_x, new_y])
    return new_points

