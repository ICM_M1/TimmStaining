import os
import numpy as np
import math
import tkinter.messagebox as messagebox
import cv2
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import image_processing as ip
import brainslice_detection as bd
import tkinter.simpledialog as simpledialog


def plotLineGraph2D(datasetY, datasetX):
    plt.plot(datasetY, datasetX)
    plt.ylabel('Densitometry')
    plt.gca().invert_yaxis()
    plt.show()

def plotHistogram2D(datasetY, datasetX):
    plt.hist(datasetY, datasetX)
    plt.ylabel('Densitometry')
    plt.show()


def plotQuantificationByRows(datasetY, datasetX, image, img_scale_factor, display_layers_img, display_layers_figure, display_layers_analysis):
    i = 0
    rowsY_buffer = datasetY.copy()
    rowsX_buffer = datasetX.copy()

    if img_scale_factor == None:
        scale_factor = 1
    else:
        scale_factor = img_scale_factor
    while i < len(datasetX):
        datasetX[i] = int(datasetX[i] * scale_factor)
        i += 1
    height, width = image.shape[:2]

    fig = plt.figure()
    ax0 = fig.add_subplot(1, 1, 1)
    image_copy = image

    if display_layers_img:
        image_copy = bd.printCortexLayersOnImage(image_copy, True)
    if display_layers_analysis:
        densito_by_layer = bd.analyseDensitometryByLayers(rowsY, rowsX)
        print(densito_by_layer)

    RGB_img = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB)
    ax0.imshow(RGB_img, aspect="equal", extent=[0, int(width * scale_factor), int(datasetX[i - 1]), 0])
    if img_scale_factor == None:
        ax0.set_ylabel('Distance(pixel)')
    else:
        ax0.set_ylabel('Distance(µm)')
    ax0.get_xaxis().set_visible(False)

    divider = make_axes_locatable(ax0)
    ax1 = divider.append_axes("right", size=1.5, pad=1, sharey=ax0)
    ax1.plot(datasetY, datasetX)
    plt.gca().set_ylim([0, int(datasetX[i - 1])])
    plt.gca().invert_yaxis()
    ax1.set_xlabel('Densitometry')
    if display_layers_figure:
        line_index = 0
        while line_index < len(bd.cortex_layers_pos):
            ax1.axhline(int(bd.cortex_layers_pos[line_index][1] * scale_factor), color='g', linewidth=1)#plots horizontal line
            line_index += 1
    if img_scale_factor == None:
        ax1.set_ylabel('Distance(pixel)')
    else:
        ax1.set_ylabel('Distance(µm)')

    if display_layers_analysis:
        densito_by_layer = bd.analyseDensitometryByLayers(rowsY_buffer, rowsX_buffer)
        #plotDensitometryTableByLayers(fig, densito_by_layer)
        writeDensitoByLayers(densito_by_layer, "data.csv")


    plt.suptitle("Densitometry of staining")#sets the title of the group of graphs
    plt.tight_layout(rect= [0, 0.03, 1, 0.95])#Packs the subplots to avoid overlapping
    plt.show()



def plotQuantificationByColumns(datasetY, datasetX, image, img_scale_factor):
    i = 0
    if img_scale_factor == None:
        scale_factor = 1
    else:
        scale_factor = img_scale_factor
    while i < len(datasetX):
        datasetX[i] = int(datasetX[i] * scale_factor)
        i += 1

    height, width = image.shape[:2]

    fig = plt.figure()
    ax0 = fig.add_subplot(1, 1, 1)
    RGB_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    ax0.imshow(RGB_img, aspect="equal", extent=[0, int(datasetX[i - 1]), int(height * scale_factor), 0])
    if img_scale_factor == None:
        ax0.set_ylabel('Distance(pixel)')
    else:
        ax0.set_ylabel('Distance(µm)')
    ax0.get_yaxis().set_visible(False)

    divider = make_axes_locatable(ax0)
    ax1 = divider.append_axes("bottom", size=1, pad=0.5, sharex=ax0)
    ax1.plot(datasetX, datasetY)
    plt.gca().set_xlim([0, int(datasetX[i - 1])])
    ax1.set_ylabel('Densitometry')
    ax1.set_ylim(ymin=0)
    if img_scale_factor == None:
        ax1.set_xlabel('Distance(pixel)')
    else:
        ax1.set_xlabel('Distance(µm)')


    plt.suptitle("Densitometry of staining")#sets the title of the group of graphs
    plt.tight_layout(rect= [0, 0.03, 1, 0.95])#Packs the subplots to avoid overlapping
    plt.show()



def plotQuantificationByRowsAndColumns(rowsY, rowsX, columnsY, columnsX, image, img_scale_factor, display_layers_img, display_layers_figure, display_layers_analysis):
    i = 0
    j = 0
    rowsY_buffer = rowsY.copy()
    rowsX_buffer = rowsX.copy()
    if img_scale_factor == None:
        scale_factor = 1
    else:
        scale_factor = img_scale_factor
    while i < len(rowsX):
        rowsX[i] = int(rowsX[i] * scale_factor)
        i += 1
    while j < len(columnsX):
        columnsX[j] = int(columnsX[j] * scale_factor)
        j += 1

    height, width = image.shape[:2]

    fig = plt.figure()
    ax0 = fig.add_subplot(1, 1, 1)
    image_copy = image
    if display_layers_img:
        image_copy = bd.printCortexLayersOnImage(image_copy, True)


    RGB_img = cv2.cvtColor(image_copy, cv2.COLOR_BGR2RGB)
    ax0.imshow(RGB_img, aspect="equal", extent=[0, int(width * scale_factor), int(height * scale_factor), 0])
    if img_scale_factor == None:
        ax0.set_ylabel('Distance(pixel)')
        ax0.set_xlabel('Distance(pixel)')
    else:
        ax0.set_ylabel('Distance(µm)')
        ax0.set_xlabel('Distance(µm)')

    divider = make_axes_locatable(ax0)
    ax1 = divider.append_axes("right", size=1.5, pad=1, sharey=ax0)
    ax1.plot(rowsY, rowsX)
    plt.gca().set_ylim([0, int(rowsX[i - 1])])
    plt.gca().invert_yaxis()
    if display_layers_figure:
        line_index = 0
        while line_index < len(bd.cortex_layers_pos):
            ax1.axhline(int(bd.cortex_layers_pos[line_index][1] * scale_factor), color='g', linewidth=1)#plots horizontal line
            line_index += 1
    ax1.set_xlabel('Densitometry')
    if img_scale_factor == None:
        ax1.set_ylabel('Distance(pixel)')
    else:
        ax1.set_ylabel('Distance(µm)')

    ax2 = divider.append_axes("bottom", size=1, pad=0.5, sharex=ax0)
    ax2.plot(columnsX, columnsY)
    plt.gca().set_xlim([0, int(columnsX[j - 1])])
    ax2.set_ylabel('Densitometry')
    if img_scale_factor == None:
        ax2.set_xlabel('Distance(pixel)')
    else:
        ax2.set_xlabel('Distance(µm)')

    if display_layers_analysis:
        densito_by_layer = bd.analyseDensitometryByLayers(rowsY_buffer, rowsX_buffer)
        #plotDensitometryTableByLayers(fig, densito_by_layer)
        writeDensitoByLayers(densito_by_layer, "data.csv")


    plt.suptitle("Densitometry of staining")#sets the title of the group of graphs
    plt.tight_layout(rect= [0, 0.03, 1, 0.95])#Packs the subplots to avoid overlapping
    plt.show()

def plotDensitometryTableByLayers(figure, densitometry_by_layers):
    ax = figure.add_subplot(2, 2, 1)
    new_densito = list()
    cortical_layers = list()
    for element in bd.cortex_layers:
        cortical_layers.append(str(element))
    for element in densitometry_by_layers:
        new_densito.append(str(element))
    rowlabels = 'Mean Intensity of Staining'
    collabels = bd.cortex_layers
    print(collabels)
    ax.table(cellText=new_densito, colLabels=collabels, rowLabels=rowlabels, loc='left')




###########################GEOMETRY##################


def calculateRectangleFrom3Points(points):
    points = points[0]
    xa, ya = points[0][0], points[0][1]
    xb, yb = points[1][0], points[1][1]
    xc, yc = points[2][0], points[2][1]
    bc = float(math.sqrt(math.pow((xc - xb), 2) + math.pow((yc - yb), 2)))
    ab = float(math.sqrt(math.pow((xb - xa), 2) + math.pow((yb - ya), 2)))
    ac = float(math.sqrt(math.pow((xc - xa), 2) + math.pow((yc - ya), 2)))

    #Calculate real C coordinates
    #cos_ab_bc = float((xb - xa)*(xc - xb) + (yb - ya) * (yc - yb))
    #print(cos_ab_bc)
    xab = xb - xa
    yab = yb - ya
    xac = xc -xa
    yac = yc - ya
    #ab_bc = math.atan2((xab*xac + yab*yac), (yab*xac - xab*yab))
    cos_ab_bc = float((math.pow(ab, 2) + math.pow(bc, 2) - math.pow(ac, 2))/(2*ab*bc))
    ab_bc = math.acos(cos_ab_bc)
    print(ab_bc)
    if ab_bc < float(math.pi/2):
        alpha = -ab_bc + math.pi/2
        cos_alpha = math.cos(alpha)
    elif ab_bc > float(math.pi/2):
        alpha = ab_bc - math.pi/2
        cos_alpha = math.cos(alpha)
    else :
        cos_alpha = 0

    new_bc = float(bc * cos_alpha)
    if yc < yb:
        new_xc = round(float((new_bc / ab) * (yb - ya)) + xb)
        new_yc = round(float((new_bc / ab) * (-xb + xa)) + yb)
    else:
        new_xc = round(float((new_bc / ab) * (-yb + ya)) + xb)
        new_yc = round(float((new_bc / ab) * (xb - xa)) + yb)

    #Calculate D coordinates
    xba, yba = (xa - xb), (ya - yb)
    xd = new_xc + xba
    yd = new_yc + yba

    rectangle = [points[0], points[1], [new_xc, new_yc], [xd, yd]]
    return rectangle

def getRotatedRectangleInfos(points):
    xa, ya = points[0][0], points[0][1]
    xb, yb = points[1][0], points[1][1]
    xc, yc = points[2][0], points[2][1]
    xd, yd = points[3][0], points[3][1]
    x_ab = xb - xa
    y_ab = yb - ya
    x_ad = xd - xa
    y_ad = yd - ya

    #Calculates center coordinates
    x_center = int(xa + 1/2*(x_ab) + 1/2 *(x_ad))
    y_center = int(ya + 1/2*(y_ab) + 1/2 *(y_ad))
    center = [x_center, y_center]

    #Calculates angle
    virtual = [x_center, 0] #point creating a vertical axis passing through the center of the rectangle
    middle_ab = [int(xa+(1/2)*x_ab), int(ya+(1/2)*y_ab)] #middle of AB
    bc = float(math.sqrt(math.pow((middle_ab[0] - center[0]), 2) + math.pow((middle_ab[1] - center[1]), 2)))
    ab = float(math.sqrt(math.pow((center[0] - virtual[0]), 2) + math.pow((center[1] - virtual[1]), 2)))
    ac = float(math.sqrt(math.pow((middle_ab[0] - virtual[0]), 2) + math.pow((middle_ab[1] - virtual[1]), 2)))
    cos_ab_bc = float((math.pow(ab, 2) + math.pow(bc, 2) - math.pow(ac, 2)) / (2 * ab * bc))
    angle = math.acos(cos_ab_bc)
    if middle_ab[0] < center[0]:
        angle = -angle

    #Calculates width and height
    width = int(math.sqrt(math.pow((xb - xa), 2) + math.pow((yb - ya), 2)))
    height = int(math.sqrt(math.pow((xd - xa), 2) + math.pow((yd - ya), 2)))

    infos = [center, angle, width, height]
    return infos

def getScaleFromScaleBar(points, value):
    xa, ya = points[0][0], points[0][1]
    xb, yb = points[1][0], points[1][1]
    ab = float(math.sqrt(math.pow((xb - xa), 2) + math.pow((yb - ya), 2)))
    scale_factor = value / ab
    return scale_factor

###########DATAS###########################

def writeDensitoByLayers(densitometry_by_layers, file_path):
    file = open(file_path, "a")
    name = simpledialog.askstring("Name of slice", "Define the name of the slice")
    line = name + ","
    i = 0
    while i < len(densitometry_by_layers)-1:
        line +=  str(densitometry_by_layers[i]) + ","
        i += 1
    line += str(densitometry_by_layers[len(densitometry_by_layers)-1]) + '\n'
    file.write(line)
    file.close()
    print(densitometry_by_layers)

