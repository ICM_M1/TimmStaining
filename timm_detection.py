import numpy as np
import argparse
import cv2
import image_processing as ip
import interface
import mathematics


def detectStaining(image):
    # Defines limits and creates numpy arrays with it
    # tuple = [(G, B, R)]
    # limits = ([17, 15, 100], [80, 80, 255])#RED
    #limits = ([220, 230, 100], [255, 255, 255])  # Narrow inverted Timm range
    #limits = ([160, 200, 90], [255, 255, 255])  # Medium inverted Timm range
    #limits = ([120, 150, 70], [255, 255, 255])  # Large inverted Timm range
    #limits = ([90, 120, 60], [255, 255, 255])  # VeryLarge inverted Timm range
    #limits = ([0, 20, 0], [255, 255, 255])  # ExtremelyLarge inverted Timm range
    #lower = ip.npArrayGeneration(limits[0])
    #upper = ip.npArrayGeneration(limits[1])
    least_stained_pixel = interface.selectPixelColor(image, "Click on the least stained pixel and press Enter")
    #least_stained_pixel = image[least_stained_pixel_pos[1], least_stained_pixel_pos[0]]
    most_stained_pixel = interface.selectPixelColor(image, "Click on the most stained pixel  and press Enter")
    #most_stained_pixel = image[most_stained_pixel_pos[1], most_stained_pixel_pos[0]]
    if ((least_stained_pixel[0] + least_stained_pixel[1] + least_stained_pixel[2])/255) > ((most_stained_pixel[0] + most_stained_pixel[1] + most_stained_pixel[2])/255):
        image = ip.invertColors(image)
        lower = ip.npArrayGeneration([255-least_stained_pixel[0], 255-least_stained_pixel[1], 255-least_stained_pixel[2]])
        upper = ip.npArrayGeneration([255-most_stained_pixel[0], 255-most_stained_pixel[1], 255-most_stained_pixel[2]])
        masked_image = ip.colorDetection(image, lower, upper)
        return masked_image, True
    else:
        lower = ip.npArrayGeneration([least_stained_pixel[0], least_stained_pixel[1], least_stained_pixel[2]])
        upper = ip.npArrayGeneration([most_stained_pixel[0], most_stained_pixel[1], most_stained_pixel[2]])
        masked_image = ip.colorDetection(image, lower, upper)
    #ip.imgShow([ip.properResizing(masked_image, 1200, 600)])
        return masked_image, False


def getMeanStainingIntensityPositivePixels(image):
    indices = np.where((image != [0, 0, 0]).all(axis=2))
    coordinates = list(zip(indices[0], indices[1]))
    i = 0
    image = ip.grayScale(image)
    for pixel in coordinates:
        color = image[pixel[0], pixel[1]]
        add = color / 255
        i += add
    i = i / len(coordinates)
    return i

def getMeanStainingIntensityAllPixels(image, number_of_pixels):
    indices = np.where((image != [0, 0, 0]).all(axis=2))
    coordinates = list(zip(indices[0], indices[1]))
    i = 0
    image = ip.grayScale(image)
    for pixel in coordinates:
        color = image[pixel[0], pixel[1]]
        add = color / 255
        i += add
    i = i / number_of_pixels
    return i

def getMeanStainingIntensityByRow(image):
    image = ip.grayScale(image)
    rows = np.arange(len(image))
    data = np.zeros(len(image))
    row_id = 0
    for row in image:
        i = 0
        for element in row:
            color = int(element)
            i += color / len(row)
        data[row_id] = i
        row_id += 1
    return data, rows

def getMeanStainingIntensityByColumns(image):
    image = ip.grayScale(image)
    height, width = image.shape[:2]
    columns = np.arange(width)
    data = np.zeros(width)

    for row in image:
        column_id = 0
        for element in row:
            color = int(element)
            data[column_id] += color / width
            column_id += 1

    return data, columns


def getStainingDensity(image, number_of_pixels):
    nb_white = ip.quantifyWhitePixels(image)
    density = nb_white / number_of_pixels
    return density

def getArea(image): #better to use with a subset of contours
    (image, contours, hierarchy) = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    total_area = 0
    for cnt in contours:
        area = cv2.contourArea(cnt)
        total_area += area
    return total_area

def processImgCommandShell(image,ROI, rotated_rect):
    image = ip.imgLoad(image)

    scale_factor = interface.selectScale(image, 1000)

    if ROI:
        ROI = interface.selectRoiByHand(image, "Select ROI by left clicks and press Enter", False)
        # ip.drawPolygon(image, ROI, [0, 255, 0], 1)
        image = ip.cropPolygon(image, ROI, True)
    elif rotated_rect:
        ROI = interface.selectRoiByHand(image, "Select ROI by doing three left clicks to define a rectangle", True)
        center, angle, width, height = mathematics.getRotatedRectangleInfos(ROI)
        image = ip.extractRotatedRectangle(image, (center[0], center[1]), angle, width, height)

    cropped = ip.properResizing(image, 1200, 600)
    cv2.imshow("Image", cropped)
    cv2.waitKey(0)


    #Counts non white pixels
    #(White pixels are assumed to be filtered background)
    non_white_pixels = ip.quantifyNonColoredPixels(image, [255, 255, 255])

    timm_filtered_image, inverted = detectStaining(cropped)
    mean_staining_intensity_pp = getMeanStainingIntensityPositivePixels(timm_filtered_image)
    mean_staining_intensity_all = getMeanStainingIntensityAllPixels(timm_filtered_image, non_white_pixels)
    enhanced_timm_filtered_image = ip.whiteEnhancement(timm_filtered_image)
    print("Number of pixels = " + str(non_white_pixels))
    print("Number of positive pixels = " + str(ip.quantifyWhitePixels(enhanced_timm_filtered_image)))
    print("Density of staining = " + str(getStainingDensity(enhanced_timm_filtered_image, non_white_pixels)))
    print("Mean Intensity of positive pixels = " + str(mean_staining_intensity_pp))
    print("Mean Intensity for all pixels = " + str(mean_staining_intensity_all))


    mean_intensity_by_rows, rows = getMeanStainingIntensityByRow(timm_filtered_image)
    mathematics.plotQuantificationByRows(mean_intensity_by_rows, rows, image, scale_factor)

    image = cv2.resize(image, (300, 200))
    image_inverted = ip.invertColors(image)
    image_inverted = cv2.resize(image_inverted, (300, 200))
    timm_filtered_image = cv2.resize(timm_filtered_image, (300, 200))
    ip.imgShow([image, image_inverted, timm_filtered_image])



#Argument Parser
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", help="image path")
ap.add_argument('--roi', help='Select a ROI by hand', action='store_true')
ap.add_argument('--rectangle', help='Select a rectangle(rotated or not)', action='store_true')
args = vars(ap.parse_args())
# if len(args) > 0:
#     processImgCommandShell(args["image"], args["roi"], args["rectangle"])






